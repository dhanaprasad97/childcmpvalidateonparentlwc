import { LightningElement, api, track } from 'lwc';

export default class ChildCmp extends LightningElement {
    connectedCallback(){

    }

    validate(){
        let allValidInput = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
          }, true);
          return allValidInput;
    }

    @api checkPaymentMethodValues() {
        if(this.validate()){
        let checkPaymentValues = {};
        checkPaymentValues.CheckCustomerIdentifier  = this.template.querySelector(`[data-field="CheckCustomerIdentifier"]`).value;
        checkPaymentValues.CheckBranchRouting  = this.template.querySelector(`[data-field="CheckBranchRouting"]`).value;
        checkPaymentValues.CheckingAccount  = this.template.querySelector(`[data-field="CheckingAccount"]`).value;
        checkPaymentValues.CheckNumber  = this.template.querySelector(`[data-field="CheckNumber"]`).value;
        return checkPaymentValues;
        } else {
            return "";
        }
    }

}