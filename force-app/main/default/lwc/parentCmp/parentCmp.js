import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class ParentCmp extends LightningElement {

    validateAndGetData(){
        let valuesForCheckPayments = {};
          valuesForCheckPayments = this.template
            .querySelector("c-child-cmp")
            .checkPaymentMethodValues();
        console.log("PAyment Values ::"+ JSON.stringify(valuesForCheckPayments));
        if(valuesForCheckPayments == ""){
            const evt = new ShowToastEvent({
                title: "Error",
                message: "Please fill all required fields",
                variant: "error"
              });
              this.dispatchEvent(evt);
        }
    }
}